## Table of contents
* [General info](#general-info)
* [Technologies](#technologies)
* [How to use](#how-to-use)

## General info
In this code an Jacobi iterative algorithm is implemented to find eigenvalues and eigenvectors of a given symmetric matrix.<br /> 

The code includes two classes

`eigenValueOfSymMatrix`: 
 In this class, an iterative algorithm to find eigenvalues and eigenvectors of symmetric matrix is implemented. It contains three constructors, eight member functions, and two operator overloading to print eigenvectors of given matrix on the screen or into a file. To create an object of the class, use one of these three constructors
 
 1-
 ```
 eigenValueOfSymMatrix::eigenValueOfSymMatrix(std::ifstream& _fileName, int _dim, bool _plotting)
 _fileName: a file in which matrix A stored 
 _dim: dimension of matrix in row or col 
 _plotting: variable which activate/deactivate plotting using gnuplot class
 ```
2-
 ```
 eigenValueOfSymMatrix::eigenValueOfSymMatrix(const T* _A, int _dim, bool _plotting)
 _A: matrix A passes as an array
 _dim:  dimension of matrix in row or col 
 _plotting: variable which activate/deactivate plotting using gnuplot class
 ```
 3-
  ```
 eigenValueOfSymMatrix::eigenValueOfSymMatrix(Eigen::Matrix _A, bool _plotting)
 _A: Eigen::Matrix object 
 _plotting: variable which activate/deactivate plotting using gnuplot class
 ```
 To applying algorithm on the given matrix use member function `symmEig(tol)` where `tol` is desired tolerance. To see eigenvalues calculated by the algorithm implemented in the class, use member function `eigenValues()`.

`gnuplot`: 
 A class used to plot given data (`||off(A)||`) as `std::vector` vs number of iteration. It has one constructor
 
 ```
 gnuplot::gnuplot()
 ```
 
 It uses [`gnuPlot`](http://www.gnuplot.info/) to plot the given data. To plot data, use member function`plot(off_A, iterationNumber)` where `off_A` is `std::vector`stores `||off(A)||` values and `iterationNumber` is number of iteration.


## Technologies
Project is created with:

g++: version 10.1.0, with level 3 of optimisation (`-o3` flag)

Eigen: version 3.3.7

## How to use
You can run the code in two following ways:

 <a name="1"></a> 1-  If you already have [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) library in your local machine, alter the path to the header file of the library in the makefile (`EIGEN_INC`) and run following four commands

```
$ mkdir data
$ mkdir obj
$ make
$ ./test
```
It creates folder `data` needed to store results, folder `obj` to store objects of compilation, then run the makefile to compile and link the library to  main source file, and run the command `./test` to execute the generated executable file. 

 <a name="2"></a>2- If you do not have the [Eigen](http://eigen.tuxfamily.org/index.php?title=Main_Page) library in you local machine, run the `bash` file

```
 $ ./Allrun.sh 
```

It automatically clones the library from [GitLab repository](https://gitlab.com/libeigen/eigen.git) in the local directory, and performs all the steps mentioned in method [1](#1) in above. 
