
# define the C++ compiler to use
CXX = g++

# flag specify compiler version
STD = -std=c++11

# flag for debugging
DBG = -g
FLAGS = -Wall -pedantic -Wno-unused-parameter

# optimisation level
OPT=-o3

# flag for profiling
#FLAGS += -pg

# path to Eigen library, alter with correct path
EIGEN_INC = ./EigenLib

SRC_DIR := ./src
OBJ_DIR := ./obj
TARGET= test1
SRC = $(TARGET).cc
SRC_OBJ = $(OBJ_DIR)/$(TARGET).o


SRC_FILES := $(wildcard $(SRC_DIR)/*.cc)
OBJ_FILES := $(patsubst $(SRC_DIR)/%.cc, $(OBJ_DIR)/%.o, $(SRC_FILES))


.PHONY: all, clean

all: $(OBJ_FILES) $(TARGET)

$(TARGET):$(SRC_OBJ) $(OBJ_FILES) 
	$(CXX) $(STD) $(OPT) $(FLAGS) -I $(EIGEN_INC) $^ -o $@

$(SRC_OBJ):$(SRC)
	$(CXX) $(STD) $(OPT) $(FLAGS) -I $(EIGEN_INC) -c $< -o $@

$(OBJ_DIR)/%.o:$(SRC_DIR)/%.cc
	$(CXX) $(STD) $(OPT) $(FLAGS) -I $(EIGEN_INC) -c $< -o $@   

clean:
	rm -rf $(TARGET) $(TARGET).o $(OBJ_DIR)/*.o

