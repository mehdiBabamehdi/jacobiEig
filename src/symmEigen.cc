

/*--------------------------------------------------------------------------

Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 08.07.2020

\*-----------------------------------------------------------------------*/



#include "../includes/symmEigen.hpp"

/*____________________________ CONSTRUCTOR ____________________________________*/

template <class T>
eigenValueOfSymMatrix<T>::eigenValueOfSymMatrix(std::ifstream& _fileName, int _dim, 
                                                bool _plotting)
{
  if(!_fileName.is_open())
  {
     std::cerr << "==> Input File is NULL!" << std::endl;
     exit(0);
  }

  dim = _dim;
  A.resize(dim, dim);
  for (int i = 0; i < dim; ++i)
  {
    for(int j = 0; j < dim; ++j)
    {
      _fileName >> A(i,j);
    }
  }
  plotting = _plotting;
  
  D = A;
  // calculation of cond(A)
  Eigen::BDCSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> 
                               svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  cond_A = svd.singularValues()(0) /
           svd.singularValues()(svd.singularValues().size()-1);
}



template <class T>
eigenValueOfSymMatrix<T>::eigenValueOfSymMatrix(const T* _A, int _dim, 
                                                bool _plotting)
{
  if(_A == NULL)
  {
     std::cerr << "==> Input Array is NULL!" << std::endl;
     exit(0);
  }

  dim = _dim;
  A.resize(dim,dim);
  for (int i = 0; i < dim; ++i)
  {
    for(int j = 0; j < dim; ++j)
    {
       A(i,j) = _A[i * dim + j];
    }
  }
  
  plotting = _plotting;
  D = A;

  // calculation of cond(A)
  Eigen::BDCSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> 
                                svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  cond_A = svd.singularValues()(0) /
                 svd.singularValues()(svd.singularValues().size()-1);

}

template <class T>
eigenValueOfSymMatrix<T>::eigenValueOfSymMatrix(const Eigen::Matrix<T, Eigen::Dynamic,Eigen::Dynamic>& _A, 
                                                bool _plotting)
{
   A = _A;
   dim = A.rows();
   plotting = _plotting;
   D = A;

  // calculation of cond(A)
  Eigen::BDCSVD<Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>> 
                               svd(A, Eigen::ComputeThinU | Eigen::ComputeThinV);
  cond_A = svd.singularValues()(0) /
                    svd.singularValues()(svd.singularValues().size()-1);
}




/*____________________________ DESTRUCTOR ____________________________________*/
template <class T>
eigenValueOfSymMatrix<T>::~eigenValueOfSymMatrix()
{
}



/*____________________________ MEMBER FUNCTION __________________________________*/
/*_____________________________________________________________________
 * @name rotate
 * @info The member function calculates c and s value according element of matrix D
 *___________________________________________________________________*/
template <class T>
void eigenValueOfSymMatrix<T>::rotate()
{
   T tau;
   T t;

   if (D(eigSymOrthMatrix.p, eigSymOrthMatrix.q) != 0.0)
   {
      tau = (D(eigSymOrthMatrix.q, eigSymOrthMatrix.q) - 
             D(eigSymOrthMatrix.p, eigSymOrthMatrix.p)) / 
             (2.0 * D(eigSymOrthMatrix.p, eigSymOrthMatrix.q));

      if (tau >= 0.0)
      {
         t = tau + sqrt(1.0 + pow(tau,2));
      } else if (tau < 0.0)
      {
         t = tau - sqrt(1.0 + pow(tau,2));
      }

      eigSymOrthMatrix.c = 1.0 / sqrt(1.0 + pow(1.0 / t, 2));
      eigSymOrthMatrix.s = eigSymOrthMatrix.c / t;
   } else
   {
      eigSymOrthMatrix.c = 1.0;
      eigSymOrthMatrix.s = 0.0;
   }
}


/*_____________________________________________________________________
 * @name createMatrixJ
 * @info The member function creates matrix J(p,q,c,s) in the algorithm
 *       by given p, q, c, s
 *___________________________________________________________________*/
template <class T>
Eigen::SparseMatrix<T> eigenValueOfSymMatrix<T>::createMatrixJ()
{
   Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic> I = 
         Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Identity(this->dim, this->dim);

   Eigen::SparseMatrix<T> matrixJ = I.sparseView();

   matrixJ.coeffRef(eigSymOrthMatrix.p, eigSymOrthMatrix.p) = eigSymOrthMatrix.c;
   matrixJ.coeffRef(eigSymOrthMatrix.q, eigSymOrthMatrix.q) = eigSymOrthMatrix.c;
   matrixJ.coeffRef(eigSymOrthMatrix.p, eigSymOrthMatrix.q) = eigSymOrthMatrix.s;
   matrixJ.coeffRef(eigSymOrthMatrix.q, eigSymOrthMatrix.p) = - eigSymOrthMatrix.s;
   
   // Since matrix J is sparse, it is made compressed to use optimised algorithm for
   // sparse matrices
   matrixJ.makeCompressed();
   return matrixJ;
}


/*_____________________________________________________________________
 * @name findMaxOfMatrixD
 * @info The member function finds max(D) | j > i
 *       
 *___________________________________________________________________*/
template <class T>
void eigenValueOfSymMatrix<T>::findMaxOfMatrixD()
{   
   T maxValue = std::abs(D(0,1));
   eigSymOrthMatrix.p = 0;
   eigSymOrthMatrix.q = 1;

   for (int i = 0; i < dim; ++i)
   {
     for (int j = i + 1; j < dim; ++j)
     {
       if (std::abs(D(i,j)) > maxValue)
       {
          maxValue = std::abs(D(i,j));
          eigSymOrthMatrix.p = i;
          eigSymOrthMatrix.q = j;
       }
     }
   }
}


/*_____________________________________________________________________
 * @name off
 * @info The member function calculates ||off(A)||, it skips zero entries
 *       and sweep strictly upper part of matrix D since it is 
 *       symmetric
 *___________________________________________________________________*/
template <class T>
T eigenValueOfSymMatrix<T>::off()
{
   T normOffDiag = 0.0;
   
   for (int i = 0; i < dim; ++i)
   {
     for (int j = i + 1; j < dim; ++j)
     {
         if (D(i,j) != 0.0)
         {
            normOffDiag += 2.0 * pow(D(i,j),2);
         }
     }
   }
   
   normOffDiag = sqrt(normOffDiag);
   return normOffDiag;
}


/*_____________________________________________________________________
 * @name symmEig
 * @info The member function implements symmEig function 
 *      
 * @para tol: tolerance which used to stopping criterion when ||off(A)|| < tol
 *___________________________________________________________________*/
template <class T>
void eigenValueOfSymMatrix<T>::symmEig(T tol)
{
   int count = 0;
   auto tt1 = std::chrono::high_resolution_clock::now();

   // timing variables
   double timeMMM    = 0.0;
   double timept     = 0.0;
   double timeMax    = 0.0;
   double timeRotate = 0.0;
   double timeOff    = 0.0;

   // a vector contains values of ||off(A)|| used for plotting
   std::vector<T> vector_off_A;

   // object of class gnuplot used for plotting data by gnuPlot
   eigSymmGnuplot = new gnuplot<T>();
   
   Eigen::SparseMatrix<T> J;
   Q = Eigen::Matrix<T, Eigen::Dynamic, Eigen::Dynamic>::Identity(this->dim, this->dim);
   
   T delta = tol * A.norm();

   T off_A = off();
   vector_off_A.push_back(off_A);

   while(off_A > delta)
   {
     count++;
     auto t1 = std::chrono::high_resolution_clock::now();
     findMaxOfMatrixD();
     auto t2 = std::chrono::high_resolution_clock::now();
     timeMax += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count() ;
     
     t1 = std::chrono::high_resolution_clock::now();
     rotate();
     t2 = std::chrono::high_resolution_clock::now();
     timeRotate += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();

     J = createMatrixJ();

     // calculation time of matrix-matrix multiplication
     t1 = std::chrono::high_resolution_clock::now();
     D = J.transpose() * D * J;
     Q = Q * J;
     t2 = std::chrono::high_resolution_clock::now();
     timeMMM += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count() ;
     
     t1 = std::chrono::high_resolution_clock::now();
     off_A = off();
     t2 = std::chrono::high_resolution_clock::now();
     timeOff += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count();

     if (plotting)
     {                
       t1 = std::chrono::high_resolution_clock::now();
       vector_off_A.push_back(off_A);
       t2 = std::chrono::high_resolution_clock::now();
       timept += std::chrono::duration_cast<std::chrono::microseconds>
                     (t2-t1).count() ;
     }
   }
   
   auto tt2 = std::chrono::high_resolution_clock::now();
   double totalTime = std::chrono::duration_cast<std::chrono::microseconds>
                     (tt2-tt1).count() ;
     
   // plot off(A) vs iteration number using gnuPlot
   if(plotting) eigSymmGnuplot->plot(vector_off_A, count);
 
   printResult(off_A, totalTime - timept, timeMMM, timeRotate, timeOff, timeMax, count);
   vector_off_A.clear();
}

/*_____________________________________________________________________
 * @name checkResult
 * @info The member function checks the  results by calculating ||A - Q D Q^T||
 *___________________________________________________________________*/
template <class T>
inline double eigenValueOfSymMatrix<T>::checkResult()
{
  return (A - Q * D * Q.transpose()).norm();
}


/*_____________________________________________________________________
 * @name eigenValues
 * @info The member function prints eigenvalues of the given matrix
 *       on the screen. It also finds  and prints eigenvalues of the given matrix by using 
 *       Eigen library.  
 *___________________________________________________________________*/
template <class T>
void eigenValueOfSymMatrix<T>::eigenValues()
{
  std::cout << "==> Eigenvalues calculated by the iterative algorithm: " << std::endl;
  for (int i = 0; i < dim; ++i)
  {
    std::cout << std::setw(7) << std::left << "lambda(" << i + 1 << ") = ";
    std::cout << std::setprecision(17) << D(i,i) << std::endl;
  }
  std::cout << std::endl;

  // eigenvalues obtained by Eigen library
  std::cout << "==> Eigenvalues calculated by Eigen library: " << std::endl;
  std::cout << A.eigenvalues() << std::endl;
  std::cout << std::endl;
}


/*_____________________________________________________________________
 * @name printResult
 * @info The member function prints the results 
 *___________________________________________________________________*/
template <class T>
void eigenValueOfSymMatrix<T>::printResult(T off_A, double totalTime, double timeMMM, 
                                     double timeRotate, double timeOff, double timeMax,
                                     int count)
{
  std::cout << "\n________________________Result_________________________\n" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left << "Algorithm";
  std::cout << std::right << ":  " << "Find Eigenvalue of Symmetric Matrix"  << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Matrix";
  std::cout << std::right << ":  " << dim << " * " << dim << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"cond(A)";
  std::cout << std::right << ":  " << cond_A << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Iteration No.";
  std::cout << std::right << ":  " << count << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Sweep No.";
  std::cout << std::right << ":  " << count * 2.0 / (dim * (dim - 1)) << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"||A - Q D Q^T||";
  std::cout << std::right << ":  " << checkResult() << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Final ||off(A)||";
  std::cout << std::right << ":  " << off_A << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Total time" ;
  std::cout << std::right  << ":  " << totalTime * 1.0e-6  << " sec" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Time: Matrix * Matrix";
  std::cout << std::right << ":  " << timeMMM / totalTime * 100.0  << " %" << std::endl;

  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Time: rotate function";
  std::cout << std::right << ":  " << timeRotate / totalTime * 100.0  << " %" << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Time: Max of A";
  std::cout << std::right << ":  " << timeMax / totalTime * 100.0  << " %" << std::endl;
  
  std::cout << std::setfill(' ') << std::setw(25);
  std::cout << std::left <<"Time: ||off(A)||";
  std::cout << std::right << ":  " << timeOff / totalTime * 100.0  << " %" << std::endl;
  
  std::cout << "\n______________________________________________________\n" << std::endl;
  
}


/*___________________________ OPERATOR OVERLOADING ______________________________*/
template <class T>
std::ofstream& operator<<(std::ofstream& ofs, const eigenValueOfSymMatrix<T>& eigClass)
{
   std::cout << "==> Writing eigenvectors into the given file" << std::endl;

   ofs << eigClass.dim << "\n";
   ofs << eigClass.dim << "\n";

   for (int i = 0; i < eigClass.dim; ++i)
   {
     for (int j = 0; j < eigClass.dim; ++j)
     {
        ofs << std::setprecision(17) << eigClass.Q(i,j) << "\n";
     }
   }
   return ofs;
}


template <class T>
std::ostream& operator<<(std::ostream& ios, const eigenValueOfSymMatrix<T>& eigClass)
{
   std::cout << "==> Eigenvectors of the given matrix" << std::endl;
   for (int i = 0; i < eigClass.dim; ++i)
   {
     for (int j = 0; j < eigClass.dim; ++j)
     {
        ios << std::setprecision(17) << std::setw(20) << eigClass.Q(i,j) << " ";
     }
     ios << "\n";
   }
   ios << "\n\n";
   
   return ios;
}
