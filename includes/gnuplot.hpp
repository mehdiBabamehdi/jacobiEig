
/*-----------------------------------------------------------------------*\
Author

    Mehdi Baba Mehdi
    mehdi.baba_mehdi@uni-wuppertal.de

Date  
    Fri 07.07.2020
\*-----------------------------------------------------------------------*/


#ifndef GNUPLOT_HPP
#define GNUPLOT_HPP

#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <cmath>
#include <vector>


/*************************************************************\
  @name gnuplot
  @info A class to plot given data as "std::vector" vs number of iteration
               by using gnuPlot.
      
\************************************************************/      
template <class T>
class gnuplot
{
   private:
    std::string gnuplotFile = "./data/plot.gnu";
    std::string dataFile = "./data/data.dat";
    std::ostringstream gplot;
    std::ofstream gplotFileStream;
    std::ofstream dataFileStream;

  public:
    gnuplot();

    ~gnuplot();

    void plot(const std::vector<T> vector_off_A, const int& iterationNumber);

};

#endif
